#!/bin/env python
'''this script removes the last line of a file if:
    - filename contains ".log"
    - filename does not contain "zzz"
    - file has a least 5 lines
    - last 15 characters including new line include 'IGNORED'
'''
import os

logfiles = [ f for f in os.listdir('.') if '.log' in f and not 'zzz' in f ]

for logfile in logfiles:
    print "logfile:", logfile
    with open(logfile, 'r') as f:
        f.seek(0, os.SEEK_END)
        nlines = f.tell()
        if nlines<5: continue
        print "nlines:", nlines
        chars=[]
        for i in range(15):
            f.seek(nlines-i)
            chars.append(f.read(1))
        lastpartoffile = "".join(chars[::-1])
        print "last part of file:", lastpartoffile
        if 'IGNORED' in lastpartoffile:
            print "IGNORED in ", logfile
            f.seek(0)
            totalfile = f.readlines()
        else:
            continue
    with open(logfile, 'w') as f:
        f.writelines(totalfile[:-1])