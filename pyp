#!/bin/env python
'''This will be a Pretty YAML Printer'''

import sys
import yaml
from pprint import pprint
filename = sys.argv[1]

with open(filename, 'r') as f:
    data = yaml.load(f)

def pythonify(json_data):
    '''tries to make keys integers (recursively!) '''
    for key, value in json_data.iteritems():
        if isinstance(value, list):
            value = [ pythonify(item) if isinstance(item, dict) else item for item in value ]
        elif isinstance(value, dict):
            value = pythonify(value)
        try:
            newkey = int(key)
            del json_data[key]
            key = newkey
        except ValueError:
            pass
        json_data[key] = value
    return json_data

data = pythonify(data)

print yaml.dump(data)