#!/bin/bash
export s=$1
# /anaconda3/envs/py27/bin/python << END
env python2.7 << END
import sys, os
from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit.Chem import Draw
filename = os.environ['s']
if not filename: filename = "mols.smi"
print "filename:", filename
try:
    suppl = Chem.SmilesMolSupplier(filename)
    mols  = [ mol for mol in suppl if mol is not None ]
except IOError:
    try:
        mols = [ Chem.MolFromSmiles(filename) ]
    except IndexError:
        raise SystemExit('no smiles given')
for mol in mols: tmp=AllChem.Compute2DCoords(mol)
img=Draw.MolsToGridImage(
    mols,
    subImgSize=(200, 200),
    molsPerRow=20, useSVG=False)
img.save('mols.png')
print "picture with {} mols saved as mols.png".format(len(mols))
END
open mols.png